import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;
/**
* A class that controls events from a view.
* @author Babafemi Adeniran
* @version 1.0
* @since March 22, 2018
*/
public class ControllerClientManagementSystem{
  /**
  * The GUI that the controller will receive events from
  */
  private ViewClientManagementSystem view;
  /**
  * The model that will be updated and will be used to update the view
  */
  private ModelClientManagementSystem model;

  /**
  * Constructs a ControllerClientManagementSystem based on the specified parameters
  * and sets up the listeners for the view
  * @param v the desired view
  * @param m the desired model
  */
  ControllerClientManagementSystem(ViewClientManagementSystem v, ModelClientManagementSystem m){
    view = v;
    model = m;
    setUpListeners();
  }

  /**
  * Sets up the listeners for the view
  */
  private void setUpListeners(){
    view.addSearchListener(new SearchListener());
    view.addClearSearchListener(new ClearSearchListener());
    view.addListListener(new ListListener());
    view.addClearListener(new ClearListener());
    view.addDeleteListener(new DeleteListener());
    view.addAddOrSaveListener(new AddOrSaveListener());
    view.addAddFromFileListener(new AddFromFileListener());
  }

  /**
  * The listener for the search button
  */
  class SearchListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			String s = view.getParameterFieldText();
      String opt = view.getOptGroupSelection();
      ArrayList<Client> resultList = new ArrayList<>();
      // Make sure an option is selected
      if (opt == null){
        return;
      }
      // Make sure there's something in the parameter field
      if (s.equals("")){
        return;
      }
      // Check which option is selected and search
      if (opt.equals("Client ID")){
        if (isValidID(s)){
          Client res = model.searchByID(Integer.parseInt(s));
          resultList.add(res);
        }
      }
      else if (opt.equals("Last Name")){
        resultList = model.searchByLastName(s);
      }
      else if (opt.equals("Client Type")){
        if (isValidType(s.charAt(0))){
          resultList = model.searchByType(s.charAt(0));
        }
      }
      // Clear list and update based on search results
      DefaultListModel<Client> m = view.getListModel();
      m.clear();
      for(Client e : resultList){
        m.addElement(e);
      }
		}

    /**
    * Checks if ID enterd is valid
    * @param s id to be checked
    * @return true if the id is valid, else false
    */
    protected boolean isValidID(String s){
      // Make sure it's a number
      try{
        Integer.parseInt(s);
      }catch(NumberFormatException e){
        view.displayErrorMessage("Please enter a valid ID: \nA number greater than or equal to 0");
        return false;
      }
      // Make sure it's a non-negative number
      if (new Integer(Integer.parseInt(s)) < 0){
        view.displayErrorMessage("Please enter a valid ID: \nA number greater than or equal to 0");
        return false;
      }
      return true;
    }
    /**
    * Checks if type enterd is valid
    * @param type type to be checked
    * @return true if the type is valid, else false
    */
    protected boolean isValidType (char c){
      if (c != 'C' && c!= 'R'){
        view.displayErrorMessage("Please enter a valid type: \n'R' or 'C'");
        return false;
      }
      else{
        return true;
      }
    }
	}

  /**
  * The listener for the clear search button
  */
  class ClearSearchListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
      DefaultListModel<Client> m = view.getListModel();
      m.clear();
		}
	}

  /**
  * The listener for the add or save button
  */
  class AddOrSaveListener extends SearchListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
      String id = view.getClientIDFieldText();
      String fName = view.getFirstNameFieldText();
      String lName = view.getLastNameFieldText();
      String addr = view.getAddressFieldText();
      String pCode = view.getPostalCodeFieldText();
      String pNum = view.getPhoneFieldText();
      char type = view.getTypeSelection();
      // Know what type of action should be taken
			if (arg0.getActionCommand().equals("Add")){
        if (!checkValidInput(id, type, pCode, pNum, fName, lName, addr))
          return;

        Client in = new Client(Integer.parseInt(id), fName, lName, addr, pCode, pNum, type);
        if (!model.insertClient(in)){
          view.displayErrorMessage("This client already exists");
        }
        else{
          // Refresh list
          SearchListener update = new SearchListener();
          update.actionPerformed(new ActionEvent(new JButton(), 0, null));
          // Switch to being able to edit client
          view.switchToEdit();
        }
      }
      else{
        if (!checkValidInput(id, type, pCode, pNum, fName, lName, addr)){
          JList<Client> list = view.getList();
          Client sel = list.getSelectedValue();
          // Make sure the client still exists and reupdate fields
          if (sel != null)
            view.setFields(sel);
          return;
        }
        // Update the client info in model
        Client up = new Client(Integer.parseInt(id), fName, lName, addr, pCode, pNum, type);
        if (!model.updateById(Integer.parseInt(id), up)){
          view.displayErrorMessage("Unable to update client");
        }
        else{
          // Refresh list
          SearchListener update = new SearchListener();
          update.actionPerformed(new ActionEvent(new JButton(), 0, null));
        }
      }
		}
    /**
    * Checks if the data in textfields is valid
    * @param id ID to be checked
    * @param type type to be checked
    * @param pCode the postal code to be checked
    * @param pNum the phone number to be checked
    * @param fName the first name to be checked
    * @param lName the last name to be checked
    * @param addr the address to be checked
    * @return true if the inputs are all valid, else false
    */
    protected boolean checkValidInput(String id, char type, String pCode, String pNum, String fName, String lName, String addr){
      boolean returning = true;
      if (!isValidID(id)){
        returning = false;
      }
      if(!isValidType(type)){
        returning = false;
      }
      if(pCode == null){
        view.displayErrorMessage("Please enter a valid postal code. For example:\n" +
        "A1A 1A1");
        returning = false;
      }
      if (pNum == null){
        view.displayErrorMessage("Please enter a valid phone number. For example:\n" +
        "111 111 1111");
        returning = false;
      }
      if(fName == null || lName == null){
        view.displayErrorMessage("Please enter a name");
        returning = false;
      }
      if (addr == null){
        view.displayErrorMessage("Please enter an address");
        returning = false;
      }
      return returning;
    }
	}

  /**
  * The listener for the delete button
  */
  class DeleteListener extends AddOrSaveListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			String id = view.getClientIDFieldText();
      if (!model.deletebyID(new Integer(Integer.parseInt(id)))){
        view.displayErrorMessage("Unable to delete client");
      }
      else{
        // Refresh list
        SearchListener update = new SearchListener();
        update.actionPerformed(new ActionEvent(new JButton(), 0, null));
      }
      // Clear all the fields and deselect selection in list
      view.clearFields();
      JList<Client> list = view.getList();
      list.clearSelection();
		}
	}

  /**
  * The listener for the clear button
  */
  class ClearListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
      JList<Client> list = view.getList();
      list.clearSelection();
			view.clearFields();
      // Just cleared, so allow for adding of new client
      view.switchToAdd();
		}
	}

  /**
  * The listener for the list
  */
  class ListListener implements ListSelectionListener
	{
		@Override
    public void valueChanged(ListSelectionEvent e) {
      if (e.getValueIsAdjusting() == false) {
        JList<Client> list = view.getList();
        Client sel = list.getSelectedValue();
        // Make sure client exists
        if (sel != null)
          view.setFields(sel);
        // Switch to editing
        view.switchToEdit();
      }
    }
	}

  /**
  * The listener for the add from file button
  */
  class AddFromFileListener extends AddOrSaveListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
      // Get a filename and make a tree based in the data in the file
			String filename = null;
			filename = JOptionPane.showInputDialog("Enter the file name: ");
			if (filename != null) {
				addFromFile(filename);
			}
		}
    /**
    * Adds data from file to model
    * @param filename the name of the file
    */
    private void addFromFile(String filename){
      try {
    	  File f = new File(filename);
        // If the file doesn't exist, show error message and return
    	  if (!f.exists()) {
    		  view.displayErrorMessage("This file does not exist.");
    		  return;
    	  }
        // Read file and insert data to the tree
    	  FileReader fReader = new FileReader(f.getName());
        BufferedReader bReader = new BufferedReader(fReader);

        String currentLine; // Will contain the current line of the file

        int i = 0;
        while ((currentLine = bReader.readLine()) != null){
    	  	String [] read = currentLine.split(";");
    	  	if (read.length != 6) {
    	  		view.displayErrorMessage("Invalid data. Please check file contents for valid input");
  			    return;
    	  	}

          String fName = read[0];
          String lName = read[1];
          String addr = read[2];
          String pCode = read[3];
          String pNum = read[4];
          char type = read[5].charAt(0);

          if (!checkValidInput(String.valueOf(i), type, pCode, pNum, fName, lName, addr)){
            view.displayErrorMessage("Invalid data. Please check file contents for valid input");
            return;
          }

          // Trying to find an ID that hasn't been used
          while(true){
            Client in = new Client(i++, fName, lName, addr, pCode, pNum, type);
            if (model.insertClient(in)){
              SearchListener update = new SearchListener();
              update.actionPerformed(new ActionEvent(new JButton(), 0, null));
              break;
            }
          }
        }
        // Close I/O
        bReader.close();
        fReader.close();
	     }catch(IOException e) {
		  view.displayErrorMessage("Something went wrong while reading the file.");
	     }
    }
	}
}
