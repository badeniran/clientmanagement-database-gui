/**
* Allows for the use of the MVC design pattern for the client management system
* @author Babafemi Adeniran
* @version 1.0
* @since March 22, 2018
*/
public class MVCClientManagementSystem{
  public static void main(String[] args) {
    ViewClientManagementSystem view = new ViewClientManagementSystem();
    ModelClientManagementSystem model = new ModelClientManagementSystem();
    model.createDatabase();
    model.createTable();
    ControllerClientManagementSystem controller = new ControllerClientManagementSystem(view, model);
    view.setVisible(true);
  }
}
