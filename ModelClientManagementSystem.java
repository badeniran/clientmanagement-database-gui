
import java.sql.*;
import java.util.ArrayList;
/**
 * A class that manages clients that might use a service in a database
 * @author Abdulkareem Dolapo
 * @since March 22, 2018
 * @version 1.0
 */
public class ModelClientManagementSystem{
	/**
	 * Database URL
	 */
	private final String DB_URL = "jdbc:mysql://localhost:3306";
	/**
	 * Database User name
	 */
	private final String USER_NAME = "username"; //Fix
	/**
	 * Database Password
	 */
	private final String PASSWORD = "password"; //FIx
	/**
	 * Database (to be ccreated) name
	 */
	private final String DB_NAME = "ClientDB";
	/**
	 * Table name
	 */
	private final String TABLE_NAME = "ClientTable";
	/**
	 * Connection object to database
	 */
	private Connection DBConnection;
	/**
	 * Stataement object used to prepare SQL statements
	 */
	private Statement statement;

	/**
	 * Constructor
	 */
	public ModelClientManagementSystem() {
		try {
			Driver driver = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(driver);

			//Get Connection
			DBConnection = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
		}
		catch(SQLException e) {
			System.err.println("COULD NOT CONNECT TO DATABASE. Make sure url, username, and"
								+ "password are correct");
			System.exit(0);
		}
	}

	/**
	 * Method creates a data base with the name specified by DB_NAME
	 */
	public void createDatabase() {
		try {
			String query = "CREATE DATABASE " + DB_NAME;
			statement = DBConnection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Successfully created DB: " + DB_NAME);
		}
		catch(SQLException e) {
			System.out.println("COULD NOT CREATE DATABASE. Be sure that database doesn't" +
								" already exist");
		}
	}

	/**
	 * Function creates table with the name specified by TABLE_NAME above
	 */
	public void createTable() {
		try {
			String query = "CREATE TABLE " +
						   DB_NAME + "." + TABLE_NAME +
						   "(" +
						   "ID INT(4) NOT NULL, " +
						   "FIRSTNAME VARCHAR(20) NOT NULL, " +
						   "LASTNAME VARCHAR(20) NOT NULL, " +
						   "ADDRESS VARCHAR(50) NOT NULL, " +
						   "POSTAL_CODE CHAR(7) NOT NULL, " +
						   "PHONE_NUMBER CHAR(12) NOT NULL, " +
						   "CLIENT_TYPE CHAR(1) NOT NULL, " +
						   "PRIMARY KEY ( id )" +
						   ")";
			statement = DBConnection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Successfully created table");
		}
		catch(SQLException e) {
			System.out.println("FAILED TO CREATE TABLE. Please be sure table does not already exist");
		}
	}

	/**
	 * Inserts client information into database
	 * @param client Client to be inserted
	 * @return true if insertion was successful, false otherwise
	 */
	public boolean insertClient(Client client) {
		String query= "INSERT INTO " +
					  DB_NAME + "." + TABLE_NAME +
					  " values (" +
					  client.getId() +", '" +
					  client.getFirstName() + "', '" +
					  client.getLastName() +"', '" +
					  client.getAddress() +"', '" +
					  client.getPostalCode() +"', '" +
					  client.getPhoneNumber() +"', '" +
					  client.getType() +"'" +")";
		try {
			statement = DBConnection.createStatement();
			statement.executeUpdate(query);
			return true; //Success
		}
		catch(SQLException e) {
			System.err.println("FALED TO INSERT NEW ROW INTO TABLE");
			return false; //Failure, ID already exists
		}

	}

	/**
	 * Searches database for a client that matches the specified id
	 * @param id Specified id
	 * @return Client Object with id if object exists, null otherwise
	 */
	public Client searchByID(int id) {
		Client foundClient = null;
		String query = "SELECT * FROM " + DB_NAME + "." + TABLE_NAME +" WHERE ID =" + id;
		try {
			statement = DBConnection.createStatement();
			ResultSet results = statement.executeQuery(query);

			if(results.next()) {
				foundClient = new Client(results.getInt("ID"),
										 results.getString("FIRSTNAME"),
										 results.getString("LASTNAME"),
										 results.getString("ADDRESS"),
										 results.getString("POSTAL_CODE"),
										 results.getString("PHONE_NUMBER"),
										 results.getString("CLIENT_TYPE").toCharArray()[0]);//Success
			}
			else
				foundClient = null; //failure
		}
		catch(SQLException e) {
			System.err.println("Error in search");
			e.printStackTrace();
		}

		return foundClient;
	}

	/**
	 * Searches database for all clients whose last name match the specified name
	 * @param name Specified name
	 * @return List of client objects whose last name matches the parameter passed in, null if no such client
	 */
	public ArrayList<Client> searchByLastName(String name) {
		ArrayList<Client> foundClients = new ArrayList<Client>();
		Client foundClient;
		String query = "SELECT * FROM " + DB_NAME + "." + TABLE_NAME +" WHERE LASTNAME='" + name + "'";
		try {
			statement = DBConnection.createStatement();
			ResultSet results = statement.executeQuery(query);

			while(results.next()) {
				foundClient = new Client(results.getInt("ID"),
										 results.getString("FIRSTNAME"),
										 results.getString("LASTNAME"),
										 results.getString("ADDRESS"),
										 results.getString("POSTAL_CODE"),
										 results.getString("PHONE_NUMBER"),
										 results.getString("CLIENT_TYPE").toCharArray()[0]);
				foundClients.add(foundClient);
			}
			return foundClients; //Success
		}
		catch(SQLException e) {
			System.err.println("Error in search");
			e.printStackTrace();

		}

		return null; //Failure
	}

	/**
	 * Searches database for all clients whose type name match the specified type
	 * @param type Specified type
	 * @return List of client objects whose last type matches the parameter passed in, null if no such client
	 */
	public ArrayList<Client> searchByType(char type) {
		ArrayList<Client> foundClients = new ArrayList<Client>();
		Client foundClient;
		String query = "SELECT * FROM " + DB_NAME + "." + TABLE_NAME +" WHERE CLIENT_TYPE='" + type + "'";
		try {
			statement = DBConnection.createStatement();
			ResultSet results = statement.executeQuery(query);

			while(results.next()) {
				foundClient = new Client(results.getInt("ID"),
										 results.getString("FIRSTNAME"),
										 results.getString("LASTNAME"),
										 results.getString("ADDRESS"),
										 results.getString("POSTAL_CODE"),
										 results.getString("PHONE_NUMBER"),
										 results.getString("CLIENT_TYPE").toCharArray()[0]);
				foundClients.add(foundClient);
			}
			return foundClients;
		}
		catch(SQLException e) {
			System.err.println("Error in search");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Deletes the client whose id matches the specified id parameter
	 * @param id Specified id parameter
	 * @return true if the delete worked, else false
	 */
	public boolean deletebyID(int id) {
		String query = "DELETE FROM " + DB_NAME + "." + TABLE_NAME + " WHERE ID=" + id;
		try {
			statement = DBConnection.createStatement();
			statement.executeUpdate(query);
		}
		catch(SQLException e) {
			System.err.println("UNABLE TO DELETE TABLE ROW");
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * Updates the client whose id marches the specified id parameter
	 * @param id specified id parameter
	 * @param updateData data to update client with
	 * @return true if the update worked, else false
	 */
	public boolean updateById(int id, Client updateData) {
		String query = "UPDATE " + DB_NAME + "." + TABLE_NAME +
					" SET " +
					"ID=" + updateData.getId() + ", " +
					"FIRSTNAME=" + "'" + updateData.getFirstName() + "', " +
					"LASTNAME=" +  "'" + updateData.getLastName() + "', " +
					"ADDRESS= " +  "'" + updateData.getAddress() + "', " +
					"POSTAL_CODE= " + "'" + updateData.getPostalCode() + "', " +
					"PHONE_NUMBER= " + "'" +updateData.getPhoneNumber() + "', " +
					"CLIENT_TYPE= '" + updateData.getType() + "'"+
					" WHERE ID=" + id;
		try {
			statement = DBConnection.createStatement();
			statement.executeUpdate(query);
		}
		catch(SQLException e) {
			System.err.println("COULD NOT PERFORM ROW UPDATE");
			e.printStackTrace();
			return false;
		}

		return true;
	}
}
