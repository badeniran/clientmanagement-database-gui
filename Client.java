/**
 * A class to store client information
 * @author Abdulkareem Dolapo
 * @since March 23, 2018
 * @version 1.0
 */
public class Client {
	/**
	 * Unique client ID
	 */
	private int id;
	/**
	 * Client's first name
	 */
	private String firstName;
	/**
	 * Cleint's last name
	 */
	private String lastName;
	/**
	 * Cleint's address
	 */
	private String address;
	/**
	 * Client's postal code
	 */
	private String postalCode;
	/**
	 * Client's phone number
	 */
	private String phoneNumber;
	/**
	 * Client's type
	 */
	private char type;

	/**
	 * Constructor
	 * @param id id
	 * @param firstName first name
	 * @param lastName last name
	 * @param address address
	 * @param postalCode postal code
	 * @param phoneNumber phone number
	 * @param type type
	 */
	public Client(int id, String firstName, String lastName, String address,
				  String postalCode, String phoneNumber, char type) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.postalCode = postalCode;
		this.phoneNumber = phoneNumber;
		this.type = type;
	}

	/**
	 * Fetches client ID
	 * @return ID
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets client ID to specified ID
	 * @param id specified ID
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Fetches client first name
	 * @return first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets client first name to specified first name
	 * @param firstName specified first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Fetches client last name
	 * @return last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets client last name to  specified last name
	 * @param lastName specified last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Fetches client address
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets client address to specified address
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Fetches client postal code
	 * @return postal code
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * Sets client postal code to specified postal code
	 * @param postalCode specified postal code
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Fetches client phone number
	 * @return phone number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Sets client phone number to specified phone number
	 * @param phoneNumber specified phone Number
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Fetches client type
	 * @return type
	 */
	public char getType() {
		return type;
	}

	/**
	 * Sets client type to specified type
	 * @param type specified type
	 */
	public void setType(char type) {
		this.type = type;
	}

	/**
	 * Compresses client information into a single string
	 * @return Client informartion
	 */
	public String toString() {
		String s = id + " " + firstName + " " + lastName + " " + type;
		return s;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
