import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
* Implements the view for the client Management system.
* @author Babafemi Adeniran
* @version 1.0
* @since March 22, 2018
*/
public class ViewClientManagementSystem extends JFrame{

  /**
  * The field where the user will input the desired parameter for searching
  */
  private JTextField parameterField = new JTextField(10);
  /**
  * The list that will display the search result
  */
  private JList<Client> list = new JList<>();
  /**
  * The list model that will be for the list
  */
  private DefaultListModel <Client> model = new DefaultListModel<>();

  /**
  * The field where the user will input the desired client information
  */
  private JTextField clientIDField = new JTextField(10);
  private JTextField firstNameField = new JTextField(10);
  private JTextField lastNameField = new JTextField(10);
  private JTextField addressField = new JTextField(15);
  private JTextField postalCodeField1 = new JTextField(3);
  private JTextField postalCodeField2 = new JTextField(3);
  private JTextField phoneField1 = new JTextField(2);
  private JTextField phoneField2 = new JTextField(2);
  private JTextField phoneField3 = new JTextField(3);
  private final static String [] options = {"-", "C", "R"};
  private JComboBox<String> typeDropDown = new JComboBox<>(options);

  /**
  * The options the user can select for searching
  */
  private JRadioButton clientIDOpt = new JRadioButton("Client ID");
  private JRadioButton lastNameOpt = new JRadioButton("Last Name");
  private JRadioButton clientTypeOpt = new JRadioButton("Client Type");
  private ButtonGroup optGroup = new ButtonGroup();

  /**
  * Buttons used for updating the search list
  */
  private JButton search = new JButton("Search");
  private JButton clearSearch = new JButton("Clear Search");

  /**
  * Buttons used for updating client info to the database
  */
  private JButton addOrSave = new JButton("Add");
  private JButton delete = new JButton("Delete");
  private JButton clear = new JButton("Clear");

  /**
  * Button for getting data from a file
  */
  private JButton addFromFile = new JButton("Add From File");

  /**
  * Sets up the UI for the view
  */
  public ViewClientManagementSystem(){
    setUpGUI();
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    pack();
    setLocationRelativeTo(null);
    setResizable(false);
  }

  /**
  * Sets up all portions of the UI
  */
  private void setUpGUI(){
    setUpTop();
    setUpRight();
    setUpLeft();
    setUpBottom();
  }

  /**
  * Sets up bottom portion of the UI
  */
  private void setUpBottom(){
    JPanel panel = new JPanel();
    panel.add(addFromFile);
    panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    add("South", panel);
  }

  /**
  * Sets up top portion of the UI
  */
  private void setUpTop(){
    JLabel windowTitle = new JLabel("Client Management System");
		windowTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
    windowTitle.setForeground(Color.white);
		Font g = new Font(Font.MONOSPACED, Font.BOLD, 30);
    windowTitle.setFont(g);
    JPanel topLayout = new JPanel(new FlowLayout());
    topLayout.setBackground(Color.gray);
    topLayout.add(windowTitle);
    topLayout.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    add("North", topLayout);
  }

  /**
  * Sets up right portion of the UI
  */
  private void setUpRight(){
    JPanel right = new JPanel();
    right.setLayout(new BoxLayout(right, BoxLayout.PAGE_AXIS));
    JPanel dataEntry = setUpDataEntry();
    JPanel buttons = setUpEntryButtons();

    JLabel rightTitle = new JLabel("Client Information");
    rightTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
    Font g = new Font(Font.MONOSPACED, Font.BOLD, 16);
    rightTitle.setFont(g);
    right.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    right.add(rightTitle);
    right.add(dataEntry);
    right.add(buttons);
    add("East", right);
  }

  /**
  * Sets up left portion of the UI
  */
  private void setUpLeft(){
    JPanel left = new JPanel();
    left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));
    left.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    left.add(setUpTopLeft());
    left.add(setUpBottomLeft());
    add("West", left);
  }

  /**
  * Sets up top left portion of the UI
  * @return the panel that holds all the components in top left portion of the GUI
  */
  private JPanel setUpTopLeft(){
    JPanel topLeftLayout = new JPanel(new BorderLayout());
    JLabel topLeftTitle = new JLabel("Search Clients");
    topLeftTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
    Font g = new Font(Font.MONOSPACED, Font.BOLD, 16);
    topLeftTitle.setFont(g);

    JPanel options = setUpParameterOptions();
    JPanel parameterPanel = setUpParameterPanel();

    topLeftLayout.add("North", topLeftTitle);
    topLeftLayout.add("Center", options);
    topLeftLayout.add("South", parameterPanel);
    return topLeftLayout;
  }

  /**
  * Sets up bottom left portion of the UI
  * @return the panel that holds all the components in bottom left portion of the GUI
  */
  private JPanel setUpBottomLeft(){
    list.setModel(model);
    list.setLayoutOrientation(JList.VERTICAL);
    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    list.setVisibleRowCount(10);
    JScrollPane scroller = new JScrollPane(list);
    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
    panel.add(new JLabel("Search Results:"));
    panel.add(scroller);
    return panel;
  }

  /**
  * Sets up the data entry layout of the UI
  * @return the panel that holds all the fields and labels
  */
  private JPanel setUpDataEntry(){
    JPanel dataEntry = new JPanel();
    dataEntry.setLayout(new BoxLayout(dataEntry, BoxLayout.PAGE_AXIS));

    dataEntry.add(setUpClientID());
    dataEntry.add(setUpFirstName());
    dataEntry.add(setUpLastName());
    dataEntry.add(setUpAddress());
    dataEntry.add(setUpPostalCode());
    dataEntry.add(setUpPhoneNumber());
    dataEntry.add(setUpClientType());

    return dataEntry;
  }

  /**
  * Sets up the client ID entry layout of the UI
  * @return the panel that holds the fields and labels
  */
  private JPanel setUpClientID(){
    JPanel clientID = new JPanel(new FlowLayout(FlowLayout.CENTER));
    clientID.add(new JLabel("Client ID: "));
    clientID.add(clientIDField);
    return clientID;
  }

  /**
  * Sets up the first name entry layout of the UI
  * @return the panel that holds the fields and labels
  */
  private JPanel setUpFirstName(){
    JPanel firstName = new JPanel(new FlowLayout(FlowLayout.CENTER));
    firstName.add(new JLabel("First Name: "));
    firstName.add(firstNameField);
    firstNameField.setDocument(new JTextFieldLimit(20));
    return firstName;
  }

  /**
  * Sets up the last name entry layout of the UI
  * @return the panel that holds the fields and labels
  */
  private JPanel setUpLastName(){
    JPanel lastName = new JPanel(new FlowLayout(FlowLayout.CENTER));
    lastName.add(new JLabel("Last Name: "));
    lastName.add(lastNameField);
    lastNameField.setDocument(new JTextFieldLimit(20));
    return lastName;
  }

  /**
  * Sets up the address entry layout of the UI
  * @return the panel that holds the fields and labels
  */
  private JPanel setUpAddress(){
    JPanel address = new JPanel(new FlowLayout(FlowLayout.CENTER));
    address.add(new JLabel("Address: "));
    address.add(addressField);
    addressField.setDocument(new JTextFieldLimit(50));
    return address;
  }

  /**
  * Sets up the postalCode entry layout of the UI
  * @return the panel that holds the fields and labels
  */
  private JPanel setUpPostalCode(){
    JPanel postalCode = new JPanel(new FlowLayout(FlowLayout.CENTER));
    postalCode.add(new JLabel("Postal Code: "));
    postalCode.add(postalCodeField1);
    postalCode.add(postalCodeField2);
    // Having different fields allows for easier entry checking
    postalCodeField1.setDocument(new JTextFieldLimit(3));
    postalCodeField2.setDocument(new JTextFieldLimit(3));
    return postalCode;
  }

  /**
  * Sets up the phone number entry layout of the UI
  * @return the panel that holds the fields and labels
  */
  private JPanel setUpPhoneNumber(){
    JPanel phoneNum = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
    phoneNum.add(new JLabel("Phone Number: "));
    phoneNum.add(phoneField1);
    phoneNum.add(new JLabel("-"));
    phoneNum.add(phoneField2);
    phoneNum.add(new JLabel("-"));
    phoneNum.add(phoneField3);
    // Having different fields allows for easier entry checking
    phoneField1.setDocument(new JTextFieldLimit(3));
    phoneField2.setDocument(new JTextFieldLimit(3));
    phoneField3.setDocument(new JTextFieldLimit(4));
    return phoneNum;
  }

  /**
  * Sets up the client type entry layout of the UI
  * @return the panel that holds the drop down and label
  */
  private JPanel setUpClientType(){
    JPanel clientType = new JPanel(new FlowLayout(FlowLayout.CENTER));
    clientType.add(new JLabel("Client Type: "));
    // Make it so that it's not selecting any options
    typeDropDown.setSelectedIndex(0);
    clientType.add(typeDropDown);
    return clientType;
  }

  /**
  * Sets up the buttons for data entry in the fields
  * @return the panel that holds the buttons
  */
  private JPanel setUpEntryButtons(){
    JPanel entryButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
    addOrSave.setActionCommand("Add");
    // Don't allow for deletion of a new client
    delete.setEnabled(false);
    entryButtons.add(addOrSave);
    entryButtons.add(delete);
    entryButtons.add(clear);
    return entryButtons;
  }

  /**
  * Sets up the buttons for selecting search parameter
  * @return the panel that holds the buttons and label
  */
  private JPanel setUpParameterOptions(){
    JPanel options = new JPanel();
    options.setLayout(new BoxLayout(options, BoxLayout.PAGE_AXIS));
    JLabel searchLabel = new JLabel("Select search type");
    searchLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
    JPanel radioPanel = setUpRadioButtons();
    options.add(searchLabel);
    options.add(radioPanel);

    return options;
  }

  /**
  * Sets up the layout for the components that allow for entering the parameter to search
  * @return the panel that holds the buttons and labels
  */
  private JPanel setUpParameterPanel(){
    JPanel parameterPanel = new JPanel();
    parameterPanel.setLayout(new BoxLayout(parameterPanel, BoxLayout.PAGE_AXIS));
    JLabel parameterLabel = new JLabel("Please enter the search parameter below");
    JPanel parameterSearch = new JPanel(new FlowLayout(FlowLayout.CENTER));
    parameterSearch.add(parameterField);
    parameterSearch.add(search);
    parameterSearch.add(clearSearch);
    parameterPanel.add(parameterLabel);
    parameterPanel.add(parameterSearch);

    return parameterPanel;
  }

  /**
  * Sets up the radio buttons
  * @return the panel that holds the buttons
  */
  private JPanel setUpRadioButtons(){
    clientIDOpt.setActionCommand("Client ID");
    lastNameOpt.setActionCommand("Last Name");
    clientTypeOpt.setActionCommand("Client Type");
    optGroup.add(clientIDOpt);
    optGroup.add(lastNameOpt);
    optGroup.add(clientTypeOpt);
    JPanel radioPanel = new JPanel(new GridLayout(0, 1));
    radioPanel.add(clientIDOpt);
    radioPanel.add(lastNameOpt);
    radioPanel.add(clientTypeOpt);

    return radioPanel;
  }

  /**
  * Adds an action listener to the search button
  * @param l the desired action listener
  */
  public void addSearchListener(ActionListener l){
    search.addActionListener(l);
  }

  /**
  * Adds an action listener to the clear search button
  * @param l the desired action listener
  */
  public void addClearSearchListener(ActionListener l){
    clearSearch.addActionListener(l);
  }

  /**
  * Adds an action listener to the add or save button
  * @param l the desired action listener
  */
  public void addAddOrSaveListener(ActionListener l){
    addOrSave.addActionListener(l);
  }

  /**
  * Adds an action listener to the delete button
  * @param l the desired action listener
  */
  public void addDeleteListener(ActionListener l){
    delete.addActionListener(l);
  }

  /**
  * Adds an action listener to the clear button
  * @param l the desired action listener
  */
  public void addClearListener(ActionListener l){
    clear.addActionListener(l);
  }

  /**
  * Adds an list listener to the Jlist button
  * @param l the desired list listener
  */
  public void addListListener(ListSelectionListener l){
    list.addListSelectionListener(l);
  }

  /**
  * Adds an action listener to the add from file button
  * @param l the desired action listener
  */
  public void addAddFromFileListener(ActionListener l){
    addFromFile.addActionListener(l);
  }

	/**
	* Gets value of parameterField
	* @return the text in the parameterField
	*/
	public String getParameterFieldText() {
		return parameterField.getText();
	}

	/**
	* Gets value of clientIDField
	* @return the text in the clientIDField
	*/
	public String getClientIDFieldText() {
		return clientIDField.getText();
	}

	/**
	* Gets value of firstNameField
	* @return null if there's no text otherwise text in the field
	*/
	public String getFirstNameFieldText() {
    if (firstNameField.getText().equals(""))
      return null;
		return firstNameField.getText();
	}

	/**
	* Gets value of lastNameField
	* @return null if there's no text otherwise text in the field
	*/
	public String getLastNameFieldText() {
    if (lastNameField.getText().equals(""))
      return null;
		return lastNameField.getText();
	}

	/**
	* Gets value of addressField
	* @return null if there's no text otherwise text in the field
	*/
	public String getAddressFieldText() {
    if (addressField.getText().equals(""))
      return null;
		return addressField.getText();
	}

  /**
  * Gets the selected type in drop down
  * @return the selected type in drop down
  */
  public char getTypeSelection(){
    String sel = (String)typeDropDown.getSelectedItem();
    return sel.charAt(0);
  }

	/**
	* Gets value of postalCodeFields
	* @return postalcode fields texts if it is valid. Otherwise, returns null
	*/
	public String getPostalCodeFieldText() {
		String s1 = postalCodeField1.getText();
    String s2 = postalCodeField2.getText();

    if (isValidPostalCode(s1, s2)){
      return s1 + " " + s2;
    }
    else{
      return null;
    }
	}

  /**
  * Gets value of phoneFields
  * @return phonenumber fields texts if it is valid. Otherwise, returns null
  */
  public String getPhoneFieldText() {
    String s1 = phoneField1.getText();
    String s2 = phoneField2.getText();
    String s3 = phoneField3.getText();
    if (isValidPhoneNum(s1, s2, s3)){
      return s1 + "-" + s2 + "-" + s3;
    }
    else{
      return null;
    }
  }

	/**
	* Gets value of selected item in typeDropDown
	* @return the selected item
	*/
	public String getTypeDropDownText() {
		return (String)typeDropDown.getSelectedItem();
	}

	/**
	* Gets value command of the selected radio button
	* @return  null if nothing was selected. Otherwise, the action command.
	*/
	public String getOptGroupSelection() {
    String s = null;
    try{
      s  = optGroup.getSelection().getActionCommand();
    }catch(NullPointerException e){
    }
		return s;
	}

  /**
	* Gets list model
	* @return  model
	*/
  public DefaultListModel<Client> getListModel(){
    return model;
  }

  /**
	* Gets list
	* @return list
	*/
  public JList<Client> getList(){
    return list;
  }

  /**
  * Checks if the entered postal code is valid
  * @return true if valid. Otherwise false
  */
  private boolean isValidPostalCode(String s1, String s2){
    if (s1.length() != 3 || s2.length() != 3){
      return false;
    }
    if (!(Character.isLetter(s1.charAt(0)) && Character.isLetter(s1.charAt(2))
          && Character.isLetter(s2.charAt(1)))){
            return false;
    }
    if (!(Character.isDigit(s1.charAt(1)) && Character.isDigit(s2.charAt(0))
          && Character.isDigit(s2.charAt(2)))){
            return false;
    }
    return true;
  }

  /**
  * Checks if the entered postal code is valid
  * @return true if valid. Otherwise false
  */
  private boolean isValidPhoneNum(String s1, String s2, String s3){
    if (s1.length() != 3 || s2.length() != 3 || s3.length() != 4)
      return false;
    try{
      Integer.parseInt(s1);
      Integer.parseInt(s2);
      Integer.parseInt(s3);
    }catch(NumberFormatException e){
      return false;
    }
    return true;
  }

  /**
  * Displays an error dialog based on parameter
  * @param errorMessage the message to be displayed in dialog
  */
  public void displayErrorMessage (String errorMessage)
	{
		JOptionPane.showMessageDialog(this, errorMessage, "ERROR", JOptionPane.ERROR_MESSAGE);
	}

  /**
  * Clears all fields of entry data
  */
  public void clearFields(){
    clientIDField.setText("");
    firstNameField.setText("");
    lastNameField.setText("");
    addressField.setText("");
    postalCodeField1.setText("");
    postalCodeField2.setText("");
    phoneField1.setText("");
    phoneField2.setText("");
    phoneField3.setText("");
    typeDropDown.setSelectedIndex(0);
  }

  /**
  * Sets all fields of entry data based on desired Client
  * @param c Client who's data fields should be set to
  */
  public void setFields(Client c){
    clientIDField.setText(String.valueOf(c.getId()));
    firstNameField.setText(c.getFirstName());
    lastNameField.setText(c.getLastName());
    addressField.setText(c.getAddress());
    String [] pCode = c.getPostalCode().split(" ");
    postalCodeField1.setText(pCode[0]);
    postalCodeField2.setText(pCode[1]);
    String [] pNum = c.getPhoneNumber().split("-");
    phoneField1.setText(pNum[0]);
    phoneField2.setText(pNum[1]);
    phoneField3.setText(pNum[2]);
    if (c.getType() == 'C')
      typeDropDown.setSelectedIndex(1);
    else
      typeDropDown.setSelectedIndex(2);
  }

  /**
  * Allows user to edit client data. Allows for deletion, but does not
  * allow for editing of ID. Also changes addOrSave to Save
  */
  public void switchToEdit(){
    delete.setEnabled(true);
    addOrSave.setText("Save");
    addOrSave.setActionCommand("Save");
    clientIDField.setEditable(false);
  }

  /**
  * Allows user to add new client data. Does not allow deletion. Also changes addOrSave to Add
  */
  public void switchToAdd(){
    delete.setEnabled(false);
    addOrSave.setText("Add");
    addOrSave.setActionCommand("Add");
    clientIDField.setEditable(true);
  }

  /**
  * Serves the purpose of limiting the maximal allowable text for a JTextField
  * Implementation gotten from:
  * http://www.java2s.com/Tutorial/Java/0240__Swing/LimitJTextFieldinputtoamaximumlength.htm
  */
  class JTextFieldLimit extends PlainDocument {
    private int limit;
    JTextFieldLimit(int limit) {
      super();
      this.limit = limit;
    }

    JTextFieldLimit(int limit, boolean upper) {
      super();
      this.limit = limit;
    }

    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
      if (str == null)
        return;

      if ((getLength() + str.length()) <= limit) {
        super.insertString(offset, str, attr);
      }
    }
  }
}
